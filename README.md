# Scene Visibility Annotation Plugin Implementation Guide

## Overview
This guide serves as both a user manual for the implementation of my bachelor's thesis at FE CTU. The implementation contains an Unreal Engine plugin and a showcase of three scene types with varying levels of complexity. The repository houses the plugin, which is compatible with Unreal Engine versions 5.3 and above.

## Installation
-  installation of Unreal Engine 5.3 as descripbed by the Epic Game's installation guide [here](https://www.unrealengine.com/en-US/download)

### Requirements
- Unreal Engine 5.3 or higher
- machine able to run the Engine (requirements as descripbed [here](https://dev.epicgames.com/documentation/en-us/unreal-engine/hardware-and-software-specifications-for-unreal-engine?application_version=5.4).)

### Setup
1. **Unzip the Plugin**: Start by unzipping the Unreal Engine directory.
2. **Open the Project**: Navigate to the root of the project and run the `.uproject` file.

## Using the Plugin

### Opening the Plugin
- Navigate to the following directory to access the plugin: `\Plugins\SequenceVisibilityAnnotation\Content\src`


### Running the Annotation
To successfully run the annotation, follow these steps:
1. **Configure Level Sequence**: Ensure the Level Sequence to be annotated is placed in the selected slot.
2. **Add Annotation Actor**: The Annotation LiDAR point cloud actor must be present in the scene. You can create a custom actor or use a prepared one found at: `\Plugins\SequenceVisibilityAnnotation\Content\Assets\lidar`


## Support
For further assistance or reporting issues, please file an issue directly on this repository.



